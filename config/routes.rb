Rails.application.routes.draw do
  
  devise_for :users
  resources :users do
    member do
      get :add_sum
      get :add_number
    end
  end
  
  root to: "users#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
