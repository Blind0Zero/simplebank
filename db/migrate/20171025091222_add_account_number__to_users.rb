class AddAccountNumberToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :accountNumber, :integer, default: 0
  end
end
