class CreateTransfers < ActiveRecord::Migration[5.1]
  def change
    create_table :transfers do |t|
      t.integer :amount
      t.integer :user_id, :limit => 8
      t.timestamps
    end
    add_foreign_key :transfers, :users 
  end
end
