class AddAddcashToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :addcash, :integer, default: 0
  end
end
