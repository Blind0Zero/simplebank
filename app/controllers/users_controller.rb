class UsersController < ApplicationController
    
    def index
        @user = $user = User.find_by(id: current_user.id)
    end
    
    def new
    end
    
    def add_sum
        sum = $user
        if(sum.addcash == 0)
            sum.bank_balance += 10000
            sum.addcash = 1
            sum.save
        end
        
        redirect_to users_path(sum)
    end
    
    def add_number
        num = $user
        if(num.accountNumber == 0)
            start_num = (532746598 + current_user.id)
            num.account_number = start_num
            num.accountNumber = 1
            num.save
        end
        
        redirect_to users_path(num)
    end
end
